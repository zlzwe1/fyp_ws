import matplotlib.pyplot as plt
import csv
import rospy
import rospkg
import pandas as pd
import numpy

def movingaverage(interval, window_size):
    window= numpy.ones(int(window_size))/float(window_size)
    return numpy.convolve(interval, window, 'same')

epoch = []
reward = []

rospack = rospkg.RosPack()
pkg_path = rospack.get_path('my_jackal_dqn')
reward_dir = pkg_path+'/training_results/4_8_reward.csv'

with open(reward_dir,'r') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        dictionary = dict(row)
        epoch.append(int(dictionary['epoch']))
        reward.append(int(dictionary['reward']))

reward_av = movingaverage(reward,15)

plt.plot(epoch,reward)
plt.plot(epoch,reward_av)
plt.xlabel('epoch')
plt.ylabel('reward')
plt.grid(b=True,which='minor',axis='both')
plt.show()