# Motion Planning with Deep Reinforcement Learning

# Everything runs on Ubuntu 18.04 LTS

* Install ROS Melodic: http://wiki.ros.org/melodic/Installation/Ubuntu
* Install catkin: $ sudo apt-get install ros-melodic-catkin
* Create a catkin workspace: http://wiki.ros.org/catkin/Tutorials/create_a_workspace
* If the ROS installation didn't install gazebo then follow: http://gazebosim.org/tutorials?tut=install_ubuntu&cat=install
* Install Gazebo_ros_pkgs:

    `$ sudo apt-get install ros-melodic-gazebo-ros-pkgs ros-melodic-gazebo-ros-control`
    
# Git Usage:

    
* Clone this repository into a preferred directory. The cloned repo will be your catkin workspace.
* To develop a feature: branch from master and make a pull request into develop when finished with changes.

# Branch Naming


    feature/<insert feature name here>
        for new features
    hotfix/<thing being fixed here>
        dedicated branches for squashing bugs that have been merged into in master
    refactor/<thing being refactored here>
        dedicated branch to cleaning up part of the code base


NOTE Before merging code make sure that you haven't broken anything else in the repo.

# Compiling:

* Use `$ catkin build` anywhere within workspace to build your packages. 
* If you've previously used catkin_make then you may need to delete build and devel folders from your workspace.
* For ROS to see your packages you must use command `$ source ~/fyp_ws/devel/setup.bash` where fyp_ws is the path of your workspace folder. This can be done automatically with each new terminal by adding to .bashrc: `$ echo "source ~/fyp_ws/devel/setup.bash" >> ~/.bashrc`

# Use:
    
* To launch the jackal model with sensors in jackal_race.world
    `$ roslaunch jackal_gazebo jackal_world.launch`
* To launch the jackal navigation demo
    `$ roslaunch jackal_navigation odom_navigation_demo.launch`
* To lauch robot visualization(RVIZ) 
    `$ roslaunch jackal_viz view_robot.launch config:=navigation`
    
 
